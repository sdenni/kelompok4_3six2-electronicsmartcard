<!-- Left Sidebar Start -->

<?php

session_start();
$pengguna;

if ($_SESSION != null) {
  $pengguna = $_SESSION["pengguna"];
} else {
  $pengguna = "Other";
}


$userAdmin = "admin_user";
$userSiswa = "siswa_user";
$userPetugas = "petugas_user";

 ?>

<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
       <!-- Search form -->
        <form role="search" class="navbar-form" >
            <div class="form-group">
                <input type="text" placeholder="Search" class="form-control">
                <button type="submit" class="btn search-button"><i class="fa fa-search"></i></button>
            </div>
        </form>
        <div class="clearfix"></div>
        <!--- Profile -->
        <div class="profile-info">
            <div class="col-xs-4">
              <a href="profile.html" class="rounded-image profile-image"><img src="../../images/users/user-100.jpg"></a>
            </div>
            <div class="col-xs-8">
                <div class="profile-text">Welcome <b><?php echo $pengguna;  ?></b></div>
            </div>
        </div>
        <!--- Divider -->
        <div class="clearfix"></div>
        <hr class="divider" />
        <div class="clearfix"></div>
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>

              <li class='has_sub'><a href='javascript:void(0);'><i class='icon-home-3'></i>
                <span>Dashboard</span>
                <span class="pull-right"><i class="fa fa-angle-down"></i></span></a>
                <ul>
                  <li><a href='../content/dashboard.php'><span>Dashboard - All</span></a></li>

                </ul>
              </li>

              <li class='has_sub'><a href='javascript:void(0);'><i class='icon-home-3'></i>
                <span>Berita</span>
                <span class="pull-right"><i class="fa fa-angle-down"></i></span></a>
                <ul>
                  <?php if ($pengguna == $userSiswa): ?>
                    <li><a href='../content/esc_beritainfo.php'><span>Berita Informasi - Siswa</span></a></li>
                    <li><a href='../content/esc_kalendar_agenda.php'><span>Kalendar Sekolah - Siswa</span></a></li>
                  <?php endif; ?>

                  <?php if ($pengguna == $userAdmin): ?>
                    <li><a href='../content/esc_crud_berita.php'><span>Daftar Berita Informasi - Admin</span></a></li>
                    <li><a href='../content/esc_crud_kalendar.php'><span>Kelola Kalendar - Admin</span></a></li>
                  <?php endif; ?>

                </ul>
              </li>

              <li class='has_sub'><a href='javascript:void(0);'><i class='icon-home-3'></i>
                <span>Siswa/i</span>
                <span class="pull-right"><i class="fa fa-angle-down"></i></span></a>
                <ul>
                  <?php if ($pengguna == $userSiswa): ?>
                    <li><a href='../content/esc_nilai_view.php'><span>Lihat Nilai - Siswa</span></a></li>
                    <li><a href='../content/esc_absensi_siswa.php'><span>Absensi Siswa - Siswa</span></a></li>
                    <li><a href='../content/esc_upload_tugas_siswa.php'><span>Upload Tugas - Siswa</span></a></li>
                  <?php endif; ?>
                  <?php if ($pengguna == $userAdmin): ?>
                    <li><a href='../content/esc_crud_nilai.php'><span>Kelola Nilai - Admin</span></a></li>
                  <?php endif; ?>
                </ul>
              </li>

              <li class='has_sub'><a href='javascript:void(0);'><i class='icon-home-3'></i>
                <span>Perpustakaan</span>
                <span class="pull-right"><i class="fa fa-angle-down"></i></span></a>
                <ul>
                  <?php if ($pengguna == $userSiswa): ?>
                    <li><a href='../content/esc_perpus_pinjam_siswa.php'><span>Peminjaman Buku - Siswa</span></a></li>
                  <?php endif; ?>
                  <?php if ($pengguna != $userSiswa): ?>
                    <li><a href='../content/esc_crud_perpus.php'><span>Kelola Peminjaman Buku - Perpustakaan + Admin</span></a></li>
                  <?php endif; ?>
                </ul>
              </li>

              <li class='has_sub'><a href='javascript:void(0);'><i class='icon-home-3'></i>
                <span>Pembayaran</span>
                <span class="pull-right"><i class="fa fa-angle-down"></i></span></a>
                <ul>
                  <?php if ($pengguna == $userSiswa): ?>
                    <li><a href='../content/esc_dsp_view.php'><span>Pembayaran DSP - Siswa</span></a></li>
                  <?php endif; ?>
                  <?php if ($pengguna == $userAdmin): ?>
                    <li><a href='../content/esc_dsp_bayar.php'><span>Kelola Pembayaran DSP - Admin</span></a></li>
                  <?php endif; ?>
                </ul>
              </li>

            </ul>
        </div>
    <div class="clearfix"></div>
    <div class="clearfix"></div><br><br><br>
</div>
    <div class="left-footer">
        <div class="progress progress-xs">
          <div class="progress-bar bg-green-1" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
            <span class="progress-precentage">80%</span>
          </div>

          <a data-toggle="tooltip" title="See task progress" class="btn btn-default md-trigger" data-modal="task-progress"><i class="fa fa-inbox"></i></a>
        </div>
    </div>
</div>
<!-- Left Sidebar End -->
