


      <div class="page-heading">
              <h1><i class='fa fa-table'></i> Daftar Berita</h1>
      </div>
            <!-- Page Heading End-->				<!-- Your awesome content goes here -->

      <div class="row">
        <div class="col-md-12">
          <div class="widget">
            <div class="widget-header transparent">
              <h2><strong>Toolbar</strong> Berita</h2>
              <div class="additional-btn">
                <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
              </div>
            </div>
            <div class="widget-content">
              <div class="data-table-toolbar">
                <div class="row">
                  <div class="col-md-4">
                    <form role="form">
                    <input type="text" class="form-control" placeholder="Search...">
                    </form>
                  </div>
                </div>
              </div>

              <div class="table-responsive">
                <table data-sortable class="table table-hover table-striped">
                  <thead>
                    <tr>
                      <th>Berita</th>
                      <th>Isi</th>
                    </tr>
                  </thead>

                  <tbody>
                    <tr>
                      <td><a href="http://localhost/electronicsmartcard/view/content/dashboard.php"><strong>Berita 1</strong></a></td>
                      <td>Lorem ipsum dolor sit amet, no clita consectetuer delicatissimi cum. Ne eam minim omittam. Cu mei mollis aperiam vivendum, quando doctus an est. No qui harum commune consectetuer, no equidem suscipit has, clita neglegentur ei mei. Id pro doctus iuvaret sensibus, ne est soleat percipitur honestatis, sea ea modus sententiae.</td>
                    </tr>

                    <tr>
                      <td><a><strong>Berita 2</strong></a></td>
                      <td>Lorem ipsum dolor sit amet, no clita consectetuer delicatissimi cum. Ne eam minim omittam. Cu mei mollis aperiam vivendum, quando doctus an est. No qui harum commune consectetuer, no equidem suscipit has, clita neglegentur ei mei. Id pro doctus iuvaret sensibus, ne est soleat percipitur honestatis, sea ea modus sententiae.</td>
                    </tr>

                    <tr>
                      <td><a><strong>Berita 3</strong></a></td>
                      <td>Lorem ipsum dolor sit amet, no clita consectetuer delicatissimi cum. Ne eam minim omittam. Cu mei mollis aperiam vivendum, quando doctus an est. No qui harum commune consectetuer, no equidem suscipit has, clita neglegentur ei mei. Id pro doctus iuvaret sensibus, ne est soleat percipitur honestatis, sea ea modus sententiae.</td>
                    </tr>

                    <tr>
                      <td><a><strong>Berita 4</strong></a></td>
                      <td>Lorem ipsum dolor sit amet, no clita consectetuer delicatissimi cum. Ne eam minim omittam. Cu mei mollis aperiam vivendum, quando doctus an est. No qui harum commune consectetuer, no equidem suscipit has, clita neglegentur ei mei. Id pro doctus iuvaret sensibus, ne est soleat percipitur honestatis, sea ea modus sententiae.</td>
                    </tr>

                    <tr>
                      <td><a><strong>Berita 5</strong></a></td>
                      <td>Lorem ipsum dolor sit amet, no clita consectetuer delicatissimi cum. Ne eam minim omittam. Cu mei mollis aperiam vivendum, quando doctus an est. No qui harum commune consectetuer, no equidem suscipit has, clita neglegentur ei mei. Id pro doctus iuvaret sensibus, ne est soleat percipitur honestatis, sea ea modus sententiae.</td>
                    </tr>

                  </tbody>
                </table>
              </div>

              <div class="data-table-toolbar">
                <ul class="pagination">
                  <li class="disabled"><a href="#">&laquo;</a></li>
                  <li class="active"><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="#">5</a></li>
                  <li><a href="#">&raquo;</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>

      </div>
