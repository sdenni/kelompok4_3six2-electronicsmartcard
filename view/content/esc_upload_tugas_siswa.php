<!DOCTYPE html>
<html>
    <head>

      <title>Upload Tugas</title>

      <?php  include '../layouts/_path.php' ?>

      <link href="../../assets/libs/dropzone/css/dropzone.css" rel="stylesheet" type="text/css" />
      <link href="../../assets/css/style.css" rel="stylesheet" type="text/css" />
      
    </head>
  <body class="fixed-left">
    <?php include '../layouts/_flip.php' ?>
    <?php include '../layouts/_modallogout.php' ?>

    <div id="wrapper">
      <?php include '../layouts/_rightsidemenu.php' ?>
      <?php include '../layouts/_topside.php' ?>
      <?php include '../layouts/_leftside.php' ?>

      <div class="content-page">
          <div class="content">

            <?php include 'tugas/upload-tugas-siswa.php' ?>

          </div>
      </div>

      <?php include '../layouts/_foot.php' ?>
    </div>

    <?php include '../layouts/_endpath.php' ?>

    <script src="../../assets/libs/dropzone/dropzone.min.js"></script>
  </body>
</html>
