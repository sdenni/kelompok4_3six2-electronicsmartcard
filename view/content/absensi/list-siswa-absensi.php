


      <div class="page-heading">
              <h1><i class='fa fa-table'></i>Persentase Kehadiran Anda</h1>
      </div>
            <!-- Page Heading End-->				<!-- Your awesome content goes here -->

      <div class="row">
        <div class="col-md-12">
          <div class="widget">
            <div class="widget-header transparent">
              <h2><strong>Toolbar</strong> Nilai</h2>
              <div class="additional-btn">
                <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
              </div>
            </div>


              <div class="table-responsive">
                <table data-sortable class="table table-hover table-striped">
                  <thead>
                    <tr>
                      <th>Kode Matkul</th>
                      <th>Nama Matkul</th>
                      <th>Absensi</th>
                    </tr>
                  </thead>

                  <tbody>
                    <tr>
                      <td><strong>A001BA</strong></td>
                      <td>Kalkulus</td>
                      <td>70%</td>
                    </tr>

                    <tr>
                      <td><strong>A001BA</strong></td>
                      <td>Kalkulus</td>
                      <td>100%</td>
                    </tr>

                    <tr>
                      <td><strong>A001BA</strong></td>
                      <td>Kalkulus</td>
                      <td>80%</td>
                    </tr>

                    <tr>
                      <td><strong>A001BA</strong></td>
                      <td>Kalkulus</td>
                      <td>60%</td>
                    </tr>

                    <tr>
                      <td><strong>A001BA</strong></td>
                      <td>Kalkulus</td>
                      <td>45%</td>
                    </tr>




                  </tbody>
                </table>
              </div>

            </div>
          </div>
        </div>

      </div>
